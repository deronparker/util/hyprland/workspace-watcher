use core::str;
use std::{io::Read, os::unix::net::UnixStream, path::Path, thread::sleep, time::Duration};

fn main() {
    let sig = std::env::var("HYPRLAND_INSTANCE_SIGNATURE")
        .expect("failed to get $HYPRLAND_INSTANCE_SIGNATURE");

    let socket_path = Path::new(&format!("/tmp/hypr/{}/.socket2.sock", sig)).to_path_buf();

    while !socket_path.exists() {
        eprintln!("socket {:?} does not exits", socket_path);
        sleep(Duration::new(5, 0));
    }

    let mut stream_iter = UnixStream::connect(&socket_path)
        .expect(&format!("failed to connect to socket at {:?}", socket_path))
        .bytes()
        .map(|res| res.expect("failed to get next byte"));

    // w == 119

    const BUFSIZE: usize = 100;
    let mut line = String::new();
    let mut linebroke = false;
    loop {
        let mut bytes: [u8; BUFSIZE] = [0; BUFSIZE];

        for i in 0..BUFSIZE {
            let byte = stream_iter.next().expect("failed to get next byte in iter");

            if byte == 10 {
                linebroke = true;
                break;
            }
            bytes[i] = byte;
        }

        line += str::from_utf8(&bytes).expect("failed to convert bytes to utf8 string");

        if linebroke {
            linebroke = false;

            let (event, data) = line.split_once(">>").expect("failed to split line on '>>'");

            if event == "workspacev2" {
                let (id, _) = data.split_once(",").expect("failed to split data on ','");
                println!("{}", id);
            }
            line.clear();
        }
    }
}
